%% RESOLUCION TEMPORAL MEDIANTE CINETICA PUNTUAL
%
clear all;
close all;

global beta beta_tot Lambda lambda

%% Parametros
Lambda = 4e-05; %

% Precursores
beta = [0.000247, 0.0013845, 0.001222,...
         0.0026455, 0.000832, 0.000169]; % Fraccion de precusores
lambda = [0.0127, 0.0317, 0.115,...
           0.311, 1.4, 3.87]; % Constantes de desintegracion 

beta_tot = sum(beta);


%% CONDICIONES INICIALES
%Estacionario o valores iniciales
P_0   = 3000;            % MW

c1o= beta(1) / (Lambda * lambda(1)) * P_0;
c2o= beta(2) / (Lambda * lambda(2)) * P_0;
c3o= beta(3) / (Lambda * lambda(3)) * P_0;
c4o= beta(4) / (Lambda * lambda(4)) * P_0;
c5o= beta(5) / (Lambda * lambda(5)) * P_0;
c6o= beta(6) / (Lambda * lambda(6)) * P_0;

cond_ini=[P_0, c1o, c2o, c3o, c4o, c5o, c6o]'; 

%% RESOLVER EDOs
options = odeset('RelTol',5e-14, 'AbsTol', 1e-16,'Stats','on') ;
disp('ode45 stats:')
% Simulacion Larga
[t,var]=ode45('ecuaciones_pk',[0, 15],cond_ini, options);
% Simulacion Corta
[t,var]=ode45('ecuaciones_pk',[0, 5],cond_ini, options);
P = var(:, 1);

%% Plot
hold on;
grid on;
plot(t,  P, '-', 'LineWidth', 3)
title('Cin�tica Puntual')
xlabel('Tiempo (s)');
ylabel('Potencia (MW)');

saveas(gcf, 'point_kinetics.eps', 'epsc')

