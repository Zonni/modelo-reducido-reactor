function yprima = ecuaciones(t, x)
% funcion de la ecuacion diferencial
% x = [x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11, x12]
% x = [ P, c1, c2, c3, c4, c5, c6, Tf, Tav, Tin, Thl, Ts] 
% 
global beta beta_tot Lambda lambda...

% Cinetica Puntual
   yprima(1,1) = beta_tot/Lambda * ( r(t)-1)*x(1) + ...
       lambda(1)*x(2)+lambda(2)*x(3)+lambda(3)*x(4) + ...
       lambda(4)*x(5)+lambda(5)*x(6)+lambda(6)*x(7);
   yprima(2,1) = beta(1)/Lambda*x(1) - lambda(1)*x(2);
   yprima(3,1) = beta(2)/Lambda*x(1) - lambda(2)*x(3);
   yprima(4,1) = beta(3)/Lambda*x(1) - lambda(3)*x(4);
   yprima(5,1) = beta(4)/Lambda*x(1) - lambda(4)*x(5);
   yprima(6,1) = beta(5)/Lambda*x(1) - lambda(5)*x(6);
   yprima(7,1) = beta(6)/Lambda*x(1) - lambda(6)*x(7);
   

    