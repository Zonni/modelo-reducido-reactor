clear all;
close all;

global beta beta_tot Lambda alfa_f alfa_av lambda...
    A1 A3 tau_cl tau_hl tau_s D3...
    P_0 rho Tf_0 Tav_0 Tin_0 Thl_0 Ts_0 

%% PARAMETROS 

% Cinetica Puntual
% Precusores
beta = [0.000247, 0.0013845, 0.001222,...
         0.0026455, 0.000832, 0.000169]; % Fraccion de precusores
lambda = [0.0127, 0.0317, 0.115,...
           0.311, 1.4, 3.87]; % Constantes de desintegracion 
beta_tot= sum(beta);
% Otros
Lambda= 4e-05;
rho=0.0;

% Parametros th
alfa_f = -0.1080;
alfa_av= -0.0060;
A1 = 0.27; 
A3 = 0.002; 
tau_cl = 2;
tau_hl = 1.5;
tau_s  = 4;
D3     = -0.3;

%A2= (Tf_0-Tav_0)*A1/P_0
%A4= - ((Tf_0-Tav_0)*A3)/(2*Tav_0-0.5*(3*Tin1_0-Tin2_0))

%% CONDICIONES INICIALES
%Estacionario o valores iniciales
P_0   = 3000;            % MW
Tf_0  = 820;             % Kelvin
Tav_0 = 570;             % Kelvin
Tin_0 = 550;             % Kelvin
Ts_0  = 540;             % Kelvin
Thl_0 = 2*Tav_0 - Tin_0; % Kelvin (590K)

c1o= beta(1) / (Lambda * lambda(1)) * P_0;
c2o= beta(2) / (Lambda * lambda(2)) * P_0;
c3o= beta(3) / (Lambda * lambda(3)) * P_0;
c4o= beta(4) / (Lambda * lambda(4)) * P_0;
c5o= beta(5) / (Lambda * lambda(5)) * P_0;
c6o= beta(6) / (Lambda * lambda(6)) * P_0;

cond_ini=[P_0, c1o, c2o, c3o, c4o, c5o, c6o, ...
         Tf_0, Tav_0, Tin_0, Thl_0, Ts_0]'; 


%% RESOLVER EDOs
options = odeset('RelTol',5e-14, 'AbsTol', 1e-16,'Stats','on') ;

% disp('ode23 stats:')
% [t,var]=ode23('ecuaciones',[0,10],cond_ini, options);

disp('ode45 stats:')
[t,var]=ode45('ecuaciones',[0,10],cond_ini, options);

%% Plots
set(0, 'DefaultLineLineWidth', 3); 
set(0,'defaultAxesFontSize',16)

P = var(:, 1);
Tf  = var(:, 8);
Tav = var(:, 9);
Tin = var(:, 10);
Thl = var(:, 11);
Ts  = var(:, 12);

figure 
hold on;
grid on;
plot(t, P)
xlabel('Tiempo (s)')
ylabel('Potencia (MW)')

figure 
hold on;
grid on;
plot(t, Tf)
xlabel('Tiempo (s)')
ylabel('Temperatura (K)')

figure 
hold on;
grid on;
plot(t, Tav)
plot(t, Tin)
plot(t, Thl)
plot(t, Ts)
xlabel('Tiempo (s)')
ylabel('Temperatura (K)')
legend('Tav', 'Tin', 'Thl', 'Ts')



