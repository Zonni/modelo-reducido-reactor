# Modelo Reducido de un Reactor Nuclear

Códgos de MATLAB correspondientes al artículo docente "Modelo reducido de un reactor nuclear y su utilización en las asignaturas del ámbito de la Ingeniería Nuclear" escrito por A. Vidal-Ferràndiz, S. Carlos, D. Ginestar y S. Gallardo,  enviado a la revista  Modelling in Science Education and Learning.

Los códigos ejecutables són:  
- modelo_estatico.m : El modelo sin perturbación.  
- modelo_sinfeedback.m : El modelo sin retroalimentación termohidráulica.  
- modelo_escalon.m : El modelo con una perturbación escalón.  
- modelo_rampa.m   El modelo con una perturbación rampa, r.  
