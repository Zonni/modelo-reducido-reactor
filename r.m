function out = r(t)
% Rampa de reactividad

    if t < 4.0
        out= 0.2/4*t;
    else
        out= 0.2;
    end
end